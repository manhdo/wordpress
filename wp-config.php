<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k*h3 27dmLa{Mm%kOw0jtBeB2ScnXsm(6MA+;3X`0,2SUC@|<E.hrgzVGJIc#H*%');
define('SECURE_AUTH_KEY',  '&w:l[2Gy)=AlE@EKZiY7mIKk|1Q&Jj[}e6tY/GJ1};q_6)+me&`evW{h83`H%<s1');
define('LOGGED_IN_KEY',    'd`<PtI}.2bg4}0O+ZQG@wUTW6<u6b,mq=pLr|j4EK;+7=c-e,17>no~K,I!P9dHH');
define('NONCE_KEY',        'lWmQ<~$ZDQ)y<orr.WmU >hP=w1 >DH6|OQ+2.001<t2eea[SS3l2BK07&Sp~z`o');
define('AUTH_SALT',        'uSTmy*^/6PK]`/eX9teKM,$%/4HA;;*%Z5Y!TZ@<ck&/Q|?SnY-]75CMNeR|&UR5');
define('SECURE_AUTH_SALT', 'MU>CA^?NV(?wT2*L>RJ];%n9`!|m4u]r|k] W)qS&#F2CSn]s zX[LiC$z(1y$`s');
define('LOGGED_IN_SALT',   '>,b?Bd&`~:&>b`$Ov`+fg0Z*=]Ro/&l7Cv[:`L$N(.)7jx.NP-/xpjYmb]I1b)x%');
define('NONCE_SALT',       'L)@Bz%3`=P/9{MT->,w;P~Z%|#*l%$M01/`D~Zr-|Ws)0rUF1$a#Br_cd)>a42QI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
